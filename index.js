'use strict';

const express = require("express");
const path = require("path");
const exphbs = require('express-handlebars')
const logger = require("./middleware/logger");
const members = require('./Members')

const app = express();

app.use(logger);

// Hasndlebars Middleware
app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');

// Body Parser Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Homepage Route
app.get('/', (req,res)=> res.render('index', {
    title: 'Contacts App',
    members
}));

// Static folder
app.use(express.static(path.join(__dirname, "public")));

// Members API Routes
app.use("/api/members", require("./routes/api/members"));

const PORT = process.env.PORT || 5000;
const HOST = '0.0.0.0';

app.listen(PORT, HOST);
console.log(`Server started on port ${PORT}...`)