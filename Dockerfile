# Specifying Image to work from
FROM node:latest

# Working directory
WORKDIR /usr/src/app


# Copy resourses - package.json & package-lock.json
COPY package*.json ./

RUN npm install

# Bundle source code
COPY . .

# Expose docker PORT
EXPOSE 5000

# Command that executes when Image launches:
CMD ["node", "index.js"]